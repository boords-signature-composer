import React from 'react';
import './Header.css'

const Header = function() {
  return (
    <div className='Header-content'>
      <h2 className='Header-title'>Signature composer</h2>
    </div>
  );
}

export default Header;
