import React, { Component } from 'react';
import logo from '../../../logo.png';
import './SignaturePreview.css';

class SignaturePreview extends Component {
  render() {
    const { data } = this.props;

    return (
      <div className='SignaturePreview'>
        <img src={logo} alt='Logo' className='SignaturePreview-image'/>
        <div className='SignaturePreview-data'>
          <h3 className='SignaturePreview-title'>{data.name}</h3>
          <p className='SignaturePreview-position'>{data.position}, Boords</p>
          <a href='https://boords.com/'>https://boords.com/</a>
        </div>
      </div>
    );
  }
}

export default SignaturePreview;
