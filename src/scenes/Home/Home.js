import React, { Component } from 'react';
import Header from '../../components/Header/Header';
import SignatureCreator from '../../components/SignatureCreator/SignatureCreator';
import Container from '../../components/Container/Container';
import './Home.css';

class Home extends Component {
  render() {
    return (
      <Container>
        <Header />

        <p>Fill the fields, click on <i>Copy</i>, and paste the result as your signature in your mail client.</p>

        <SignatureCreator />

        <small className='Home-message'>
          This app was built for Boords by {' '}
          <a href="http://roperzh.com" target='_blank' rel="noopener noreferrer">Roberto Dip</a>.
          Check out the {' '}
          <a href="https://gist.github.com/roperzh/967781158e1fa6bf911b9ebcfc5b0c4c" target='_blank' rel="noopener noreferrer">README</a> {' '}
          and the {' '}
          <a href="http://repo.or.cz/boords-signature-composer.git" target='_blank' rel="noopener noreferrer">code</a>.
        </small>
      </Container>
    );
  }
}

export default Home;
